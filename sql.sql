SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[file_list](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FILE_NAME] [nvarchar](255) NULL,
	[FILE_CLASS] [float] NULL,
	[FILE_NUM] [nvarchar](255) NULL,
	[FILE_YEAR] [float] NULL,
	[FILE_RESERVE_LOCATION] [float] NULL,
	[FILE_SUBJECT] [int] NULL,
	[FILE_ADD_USER] [float] NULL,
	[FILE_ADD_TIME] [datetime] NULL,
	[FILE_UPDATE_USER] [int] NULL,
	[FILE_UPDATE_TIME] [datetime] NULL,
	[FILE_KEYWORDS] [nvarchar](255) NULL,
	[FILE_REMARK] [nvarchar](255) NULL,
	[FILE_REC] [bit] NOT NULL CONSTRAINT [DF_file_list_FILE_REC]  DEFAULT ((0)),
	[FILE_VIST_NUM] [float] NULL,
	[FILE_LOCK] [bit] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[doc_list](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[file_id] [int] NULL,
	[doc_name] [nvarchar](max) NULL,
	[doc_size] [int] NULL,
	[doc_save_root] [nvarchar](255) NULL,
	[doc_cover_name] [nvarchar](255) NULL,
	[doc_add_time] [datetime] NULL,
	[doc_download_num] [int] NULL,
	[doc_upload_user] [smallint] NULL,
	[doc_rec] [bit] NULL CONSTRAINT [DF_doc_list_doc_rec]  DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tree_list](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Class_module] [float] NULL,
	[Class_name] [nvarchar](255) NULL,
	[Class_Enable] [bit] NULL,
	[Class_path] [nvarchar](255) NULL,
	[PID] [float] NULL,
	[CID] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[USER_NAME] [nvarchar](255) NULL,
	[USER_PWD] [nvarchar](255) NULL,
	[USER_ADD_TIME] [datetime] NULL,
	[USER_ADD_UID] [nvarchar](255) NULL,
	[USER_PWD_ERROR_NUM] [nvarchar](255) NULL,
	[USER_ENABLE] [bit] NOT NULL,
	[USER_lastlogintime] [datetime] NULL,
	[USER_LASTLOGINIP] [nvarchar](255) NULL,
	[USER_AU_CLASS] [nvarchar](255) NULL,
	[USER_O_AU] [nvarchar](max) NULL,
	[USER_GROUP] [nvarchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webconfig](
	[webname] [ntext] NULL,
	[copyright] [ntext] NULL,
	[notice] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usergroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [nchar](10) NULL,
	[Defperm] [ntext] NULL,
	[Desp] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[collect_list](
	[UID] [int] NULL,
	[FID] [int] NULL,
	[COLLECT_TIME] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[视图1]
AS
SELECT     dbo.tree_list.Class_name AS 存放地点, dbo.file_list.FILE_NAME, dbo.file_list.FILE_NUM, dbo.file_list.FILE_YEAR, tree_list_1.Class_name AS 类别, 
                      tree_list_2.Class_name AS 专题
FROM         dbo.file_list INNER JOIN
                      dbo.tree_list ON dbo.file_list.FILE_RESERVE_LOCATION = dbo.tree_list.ID INNER JOIN
                      dbo.tree_list AS tree_list_1 ON dbo.file_list.FILE_SUBJECT = tree_list_1.ID INNER JOIN
                      dbo.tree_list AS tree_list_2 ON dbo.file_list.FILE_CLASS = tree_list_2.ID
WHERE     (dbo.file_list.FILE_REC = 0)

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[20] 4[24] 2[29] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -413
      End
      Begin Tables = 
         Begin Table = "file_list"
            Begin Extent = 
               Top = 37
               Left = 99
               Bottom = 242
               Right = 430
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "tree_list"
            Begin Extent = 
               Top = 20
               Left = 518
               Bottom = 263
               Right = 884
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tree_list_1"
            Begin Extent = 
               Top = 39
               Left = 1042
               Bottom = 215
               Right = 1187
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tree_list_2"
            Begin Extent = 
               Top = 0
               Left = 897
               Bottom = 108
               Right = 1042
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 36
         Width = 284
         Width = 1500
         Width = 6645
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      ' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'视图1'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'   Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'视图1'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'视图1'

