﻿<div class="title">权限管理</div>
<%
uid=request("uid")
if uid=1 then
response.write ("<script>alert('不能对系统用户进行此操作！');location.href='index.asp?p=500';</script>")
end if
%>
<div>对<span style="color:#0000FF; font-weight:bold;"><%=trans("USERS","id",uid,"USER_NAME")%></span>进行授权</div>
<form method="post">
<%
set rs=server.createobject("ADODB.Recordset")  
sql ="select * from users where id= " & uid
rs.Open sql,conn,1,1
str_o=rs("USER_O_AU")
%>
<div style="border:thin #0000CC dashed; height:150px; width:361px; float:left;">
<div style="color:#660000; font-weight:bold; font-size:16px; background-color:#99CCFF; text-align:center;">系统基础管理</div>
允许修改系统参数：<input name="qx_1" type="checkbox" value="1" <%if splitstr(str_o,"|",1)=1 then response.Write("checked") %>/><br/>
允许管理用户组：<input name="qx_2" type="checkbox" value="1" <%if splitstr(str_o,"|",2)=1 then response.Write("checked") %>/><br/>
允许管理用户：<input name="qx_3" type="checkbox" value="1" <%if splitstr(str_o,"|",3)=1 then response.Write("checked") %>/><br/>
允许管理档案类别：<input name="qx_4" type="checkbox" value="1" <%if splitstr(str_o,"|",4)=1 then response.Write("checked") %>/><br/>
允许管理存放地点：<input name="qx_5" type="checkbox" value="1" <%if splitstr(str_o,"|",5)=1 then response.Write("checked") %>/><br/>
允许管理专题：<input name="qx_6" type="checkbox" value="1" <%if splitstr(str_o,"|",6)=1 then response.Write("checked") %>/><br/>
</div>
<div style="border:thin #0000CC dashed; height:150px; width:361px;float:left;">
<div style="color:#660000; font-weight:bold; font-size:16px; background-color:#99CCFF; text-align:center;">档案管理</div>
允许录入档案：<input name="qx_7" type="checkbox" value="1" <%if splitstr(str_o,"|",7)=1 then response.Write("checked") %>/><br/>
允许编辑档案：<input name="qx_8" type="checkbox" value="1" <%if splitstr(str_o,"|",8)=1 then response.Write("checked") %>/><br/>
允许查看档案详情：<input name="qx_9" type="checkbox" value="1" <%if splitstr(str_o,"|",9)=1 then response.Write("checked") %>/><br/>
允许收藏档案：<input name="qx_10" type="checkbox" value="1" <%if splitstr(str_o,"|",10)=1 then response.Write("checked") %>/><br/>
允许回收档案：<input name="qx_11" type="checkbox" value="1" <%if splitstr(str_o,"|",11)=1 then response.Write("checked") %>/><br/>
允许删除档案：<input name="qx_12" type="checkbox" value="1" <%if splitstr(str_o,"|",12)=1 then response.Write("checked") %>/><br/>
</div>
<div style="border:thin #0000CC dashed; height:150px; width:361px;float:left;">
<div style="color:#660000; font-weight:bold; font-size:16px; background-color:#99CCFF; text-align:center;">附件管理</div>
允许上传附件：<input name="qx_13" type="checkbox" value="1" <%if splitstr(str_o,"|",13)=1 then response.Write("checked") %>/><br/>
允许下载附件：<input name="qx_14" type="checkbox" value="1" <%if splitstr(str_o,"|",14)=1 then response.Write("checked") %>/><br/>
允许更名附件：<input name="qx_15" type="checkbox" value="1" <%if splitstr(str_o,"|",15)=1 then response.Write("checked") %>/><br/>
允许回收附件：<input name="qx_16" type="checkbox" value="1" <%if splitstr(str_o,"|",16)=1 then response.Write("checked") %>/><br/>
允许删除附件：<input name="qx_17" type="checkbox" value="1" <%if splitstr(str_o,"|",17)=1 then response.Write("checked") %>/><br/>
</div>
<div style="border:thin #0000CC dashed; height:150px; width:361px;float:left;">
<div style="color:#660000; font-weight:bold; font-size:16px; background-color:#99CCFF; text-align:center;">档案高级管理</div>
允许查询他人档案：<input name="qx_18" type="checkbox" value="1" <%if splitstr(str_o,"|",18)=1 then response.Write("checked") %>/><br/>
允许编辑他人档案：<input name="qx_19" type="checkbox" value="1" <%if splitstr(str_o,"|",19)=1 then response.Write("checked") %>/><br/>
允许回收他人档案：<input name="qx_20" type="checkbox" value="1" <%if splitstr(str_o,"|",20)=1 then response.Write("checked") %>/><br/>
允许删除他人档案：<input name="qx_21" type="checkbox" value="1" <%if splitstr(str_o,"|",21)=1 then response.Write("checked") %>/><br/>
</div>
<div style="border:thin #0000CC dashed; height:150px; width:361px;float:left;">
<div style="color:#660000; font-weight:bold; font-size:16px; background-color:#99CCFF; text-align:center;">附件高级管理</div>
允许查询他人附件：<input name="qx_22" type="checkbox" value="1" <%if splitstr(str_o,"|",22)=1 then response.Write("checked") %>/><br/>
允许编辑他人附件：<input name="qx_23" type="checkbox" value="1" <%if splitstr(str_o,"|",23)=1 then response.Write("checked") %>/><br/>
允许在他人档案下添加附件：<input name="qx_24" type="checkbox" value="1" <%if splitstr(str_o,"|",24)=1 then response.Write("checked") %>/><br/>
允许回收他人附件：<input name="qx_25" type="checkbox" value="1" <%if splitstr(str_o,"|",25)=1 then response.Write("checked") %>/><br/>
允许删除他人附件：<input name="qx_26" type="checkbox" value="1" <%if splitstr(str_o,"|",26)=1 then response.Write("checked") %>/><br/>
允许锁定档案：<input name="qx_27" type="checkbox" value="1" <%if splitstr(str_o,"|",27)=1 then response.Write("checked") %>/><br/>
</div>
<label style="display:none;">备用段：<input name="qx_28" type="checkbox" value="1" <%if splitstr(str_o,"|",28)=1 then response.Write("checked") %>/><br/></label>
<label style="display:none;">备用段：<input name="qx_29" type="checkbox" value="1" <%if splitstr(str_o,"|",29)=1 then response.Write("checked") %>/><br/></label>
<label style="display:none;">备用段：<input name="qx_30" type="checkbox" value="1" <%if splitstr(str_o,"|",30)=1 then response.Write("checked") %>/><br/></label>
<label style="display:none;">备用段：<input name="qx_31" type="checkbox" value="1" <%if splitstr(str_o,"|",31)=1 then response.Write("checked") %>/><br/></label>
<label style="display:none;">备用段：<input name="qx_32" type="checkbox" value="1" <%if splitstr(str_o,"|",32)=1 then response.Write("checked") %>/><br/></label>
<label style="display:none;">备用段：<input name="qx_33" type="checkbox" value="1" <%if splitstr(str_o,"|",33)=1 then response.Write("checked") %>/><br/></label>
<label style="display:none;">备用段：<input name="qx_34" type="checkbox" value="1" <%if splitstr(str_o,"|",34)=1 then response.Write("checked") %>/><br/></label>
<label style="display:none;">备用段：<input name="qx_35" type="checkbox" value="1" <%if splitstr(str_o,"|",35)=1 then response.Write("checked") %>/><br/></label>
<label style="display:none;">备用段：<input name="qx_36" type="checkbox" value="1" <%if splitstr(str_o,"|",36)=1 then response.Write("checked") %>/><br/></label>
<label style="display:none;">备用段：<input name="qx_37" type="checkbox" value="1" <%if splitstr(str_o,"|",37)=1 then response.Write("checked") %>/><br/></label>
<label style="display:none;">备用段：<input name="qx_38" type="checkbox" value="1" <%if splitstr(str_o,"|",38)=1 then response.Write("checked") %>/><br/></label>
<label style="display:none;">备用段：<input name="qx_39" type="checkbox" value="1" <%if splitstr(str_o,"|",39)=1 then response.Write("checked") %>/><br/></label>
<label style="display:none;">备用段：<input name="qx_40" type="checkbox" value="1" <%if splitstr(str_o,"|",40)=1 then response.Write("checked") %>/><br/></label>
<hr />
<div style=" width:1100px; float:left; text-align:center; margin-top:20px;"><input type="submit" name="sq" value="授权"><input type="submit" name="back" value="返回"></div>
</form>
<%rs.close%>
<%
if request("sq")="授权" then
qx_1= iif( request("qx_1")="1",1,0)
qx_2= iif( request("qx_2")="1",1,0)
qx_3= iif( request("qx_3")="1",1,0)
qx_4= iif( request("qx_4")="1",1,0)
qx_5= iif( request("qx_5")="1",1,0)
qx_6= iif( request("qx_6")="1",1,0)
qx_7= iif( request("qx_7")="1",1,0)
qx_8= iif( request("qx_8")="1",1,0)
qx_9= iif( request("qx_9")="1",1,0)
qx_10= iif( request("qx_10")="1",1,0)
qx_11= iif( request("qx_11")="1",1,0)
qx_12= iif( request("qx_12")="1",1,0)
qx_13= iif( request("qx_13")="1",1,0)
qx_14= iif( request("qx_14")="1",1,0)
qx_15= iif( request("qx_15")="1",1,0)
qx_16= iif( request("qx_16")="1",1,0)
qx_17= iif( request("qx_17")="1",1,0)
qx_18= iif( request("qx_18")="1",1,0)
qx_19= iif( request("qx_19")="1",1,0)
qx_20= iif( request("qx_20")="1",1,0)
qx_21= iif( request("qx_21")="1",1,0)
qx_22= iif( request("qx_22")="1",1,0)
qx_23= iif( request("qx_23")="1",1,0)
qx_24= iif( request("qx_24")="1",1,0)
qx_25= iif( request("qx_25")="1",1,0)
qx_26= iif( request("qx_26")="1",1,0)
qx_27= iif( request("qx_27")="1",1,0)
qx_28= iif( request("qx_28")="1",1,0)
qx_29= iif( request("qx_29")="1",1,0)
qx_30= iif( request("qx_30")="1",1,0)
qx_31= iif( request("qx_31")="1",1,0)
qx_32= iif( request("qx_32")="1",1,0)
qx_33= iif( request("qx_33")="1",1,0)
qx_34= iif( request("qx_34")="1",1,0)
qx_35= iif( request("qx_35")="1",1,0)
qx_36= iif( request("qx_36")="1",1,0)
qx_37= iif( request("qx_37")="1",1,0)
qx_38= iif( request("qx_38")="1",1,0)
qx_39= iif( request("qx_39")="1",1,0)
qx_40= iif( request("qx_40")="1",1,0)

USER_O_AU=qx_1 &"|" & qx_2 &"|" & qx_3 &"|" & qx_4 &"|" & qx_5 &"|" & qx_6 &"|" & qx_7 &"|" & qx_8 &"|" & qx_9 &"|" & qx_10 &"|" & qx_11 &"|" & qx_12&"|" & qx_13 & "|" & qx_14 &"|" &qx_15 &"|" &qx_16 &"|" &qx_17 &"|" &qx_18 &"|" &qx_19 &"|" &qx_20 &"|" &qx_21 &"|" &qx_22 &"|" &qx_23 &"|" &qx_24 &"|" &qx_25 &"|" &qx_26 &"|" &qx_27 &"|" &qx_28 &"|" &qx_29 &"|" &qx_30 &"|" &qx_31 &"|" &qx_32 &"|" &qx_33 &"|" &qx_34 &"|" &qx_35 &"|" &qx_36 &"|" &qx_37 &"|" &qx_38 &"|" &qx_39 &"|" &qx_40
set rs=server.createobject("ADODB.Recordset")  
sql ="select * from users where id= " & uid
rs.Open sql,conn,3,2
rs("USER_O_AU")=USER_O_AU
rs.update	
rs.close
response.write ("<script>alert('授权成功！');location.href='index.asp?p=504&uid="&uid&"';</script>")
end if
%>
<%if request("back")="返回" then response.write ("<script>location.href='index.asp?p=500';</script>")%>