﻿<% module=request("module")%>
<%
if module=1 then title="档案类别管理"
if module=2 then title="存放地点管理"
if module=3 then title="专题管理"
%>
<div class="title"><%=title%></div>
<style type="text/css">
a:link {color: #333333;TEXT-DECORATION:none;}
a:visited {color: #333333;TEXT-DECORATION:none;}
a:hover {color: #9900FF; font-weight:bold;TEXT-DECORATION:none;}
a:active {color: #333333;text-decoration: none;}
table{border-collapse:collapse;border-spacing:0;border-left:1px solid #888;border-top:1px solid #888; margin-bottom:15px;}  
th,td{border-right:1px solid #888;border-bottom:1px solid #888;padding:5px 5px;}  
th{font-weight:bold;background:#ccc;}  
</style>
<!--===========================================================-->
<!--======================以下代码输出类别========================-->
<!--===========================================================-->
<table width="100%" border="1">
  <tr align="center" bgcolor="#CCCCCC" style="font-weight:bolder">
    <td width="40px">ID</td>
    <td width="40px">级次</td>
    <td>类别</td>
    <td>禁用</td>
    <td>操作</td>
  </tr>
<%
Set rs = CreateObject("ADODB.RecordSet")
SQL="Select * from tree_list where class_module=" & module &" order by class_path"
rs.Open SQL,conn,1,1
%>
<%
'-------------------------------------------     
if rs.recordcount =0 then
    response.end
else 
    rs.PageSize =15 '每页记录条数 
    iCount=rs.RecordCount '记录总数 
    iPageSize=rs.PageSize 
    maxpage=rs.PageCount
    page=request("page")
    if Not IsNumeric(page) or page="" then 
        page=1 
    else 
        page=cint(page) 
    end if 
    if page<1 then 
        page=1 
    elseif  page>maxpage then 
        page=maxpage 
    end if 
    rs.AbsolutePage=Page 
    if page=maxpage then 
       x=iCount-(maxpage-1)*iPageSize 
    else 
       x=iPageSize 
    end if 
'-------------------------------------------   
%>
<%
For i=1 To x
'-------------------------------------------  
%>
<%
	c_id = rs("id")
	c_name = rs("Class_name")
	c_enable = rs("Class_enable")
	c_path = rs("Class_path")
	c_arr = split(c_path,",")
	c_num = ubound(c_arr)
	str=""
	for j=2 to c_num
		str=str&"|---"
	next 	
%>
<%if c_num-1=0 then %>
  <tr style="display:none;">
    <td align="center"><font style="color:#00F"><%=c_id%></font></td>
    <td align="center"><font style="color:#00F"><%=c_num-1%></font></td>
    <td ><font style="color:#999999;"><%=str%></font><%=c_name%></td>
    <td align="center" width="50px"><%=iff(c_enable)%></td>
    <td width="250px" align="center"><a href="index.asp?p=201&c_id=<%=C_ID%>&module=<%=module%>&page=<%=page%>&action=addnew" >添加子类</a>||<a href="index.asp?p=202&c_id=<%=C_ID%>&module=<%=module%>&page=<%=page%>&action=update" >修改</a>||<a href="index.asp?p=203&c_id=<%=C_ID%>&module=<%=module%>&page=<%=page%>&action=delete" >删除</a>||<a href="index.asp?p=203&c_id=<%=C_ID%>&module=<%=module%>&page=<%=page%>&action=enable" >启/禁用</a></td>
  </tr>
<%else%>
  <tr>
    <td align="center"><font style="color:#00F"><%=c_id%></font></td>
    <td align="center"><font style="color:#00F"><%=c_num-1%></font></td>
    <td ><font style="color:#999999;"><%=str%></font><%=c_name%></td>
    <td align="center" width="50px"><%=iff(c_enable)%></td>
    <td width="250px" align="center"><a href="index.asp?p=201&c_id=<%=C_ID%>&module=<%=module%>&page=<%=page%>&action=addnew" >添加子类</a>||<a href="index.asp?p=202&c_id=<%=C_ID%>&module=<%=module%>&page=<%=page%>&action=update" >修改</a>||<a href="index.asp?p=203&c_id=<%=C_ID%>&module=<%=module%>&page=<%=page%>&action=delete" >删除</a>||<a href="index.asp?p=203&c_id=<%=C_ID%>&module=<%=module%>&page=<%=page%>&action=enable" >启/禁用</a></td>
  </tr>
<%end if%>
<%
'------------------------------------------ 
rs.movenext 
next%>
</div>
</table>
<%
call PageControl(iCount,maxpage,page) 
end if 
rs.close
'------------------------------------------ 
%>
<%
'===============================================================
'========================'构建IIF函数=============================
'===============================================================
function iff(str)          
   if str=true then iff="启用"
   if str=false then iff="<span style='color:#FF0000'>禁用</span>"
end function
%>
