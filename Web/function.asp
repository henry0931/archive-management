﻿<%
'系统变量
Public UID '用户id
Public FID '档案ID
Public DID '附件ID
%>

<% 
'==========================================
'=============日期格式化函数===================
'timestr:时间
'============= 2018/02/02==================
function formattime(timestr)
formattime=cstr(year(timestr)) & "/"
if len(cstr(month(timestr)))=1 then
  formattime=formattime & "0" & cstr(month(timestr)) & "/"
else 
  formattime=formattime & cstr(month(timestr)) & "/"
end if
if len(cstr(day(timestr)))=1 then
  formattime=formattime & "0" & cstr(day(timestr))
else 
  formattime=formattime & cstr(day(timestr))
end if
end function
%>

<%
'==============================================
'=============类别、地点转换函数=================
'==============2018/02/18======================
function converid(n)
if not isnumeric(n) then n=0 
set rs_a=server.CreateObject("adodb.recordset")
set rs_b=server.CreateObject("adodb.recordset")
sql="Select * from tree_list where id=" & n
rs_a.Open sql,conn,1,1
if rs_a.RecordCount=1 then
c_arr = split(rs_a("Class_path"),",")
c_num = ubound(c_arr)
str=""
for x=2 to c_num
    sql ="select * from tree_list where ID = "& c_arr(x)
    set rs_b=server.CreateObject("adodb.recordset")
    rs_b.Open sql,conn,1,1
    if not rs_b.eof then
	   tt = rs_b("class_name")
    end if
    str=str &"-" & tt   
next
converid= mid(str,2,len(str)-1)
else
   converid="#ERROR#"
end if
rs_a.close
rs_b.close
set rs_a=nothing
set rs_b=nothing
end function
%>
<% 
'==========================================
'=============截取文件扩展名=================
'============= 2018/02/18==================
function Ext(Extstr)
ext_arr = split(extstr,".")
ext_num = ubound(ext_arr)
ext=ext_arr(ext_num)
end function
%>

<% 
'==========================================
'===============字符截取函数=================
'============= 2018/03/4==================
function splitstr(str,strx,n)
splitstr_arr = split(str,strx)
splitstr_num = ubound(splitstr_arr)
splitstr=splitstr_arr(n-1)
end function
%>
<% 
'==========================================
'=============判断是否添加了附件=============
'============= 2018/02/18==================
function fjpic(fj)
set rs_c=server.CreateObject("adodb.recordset")
sql_c="Select * from doc_list where file_id=" & fj &" and doc_rec=0 "
rs_c.Open sql_c,conn,1,1
   if rs_c.RecordCount<>0 then
      response.write("<img src='image/paperclip.png' width='15' height='15' /> ")
   end if
rs_c.close
set rs_c=nothing
end function
%>
<%
'======================================================
'===============把数字转换为文件大小显示方式===============
'=====================引用函数======================
Function GetSize(Size)
	If Size < 1024 Then
		GetSize = FormatNumber(Size, 2) & "B"
	ElseIf Size >= 1024 And Size < 1048576 Then
		GetSize = FormatNumber(Size / 1024, 2) & "KB"
	ElseIf Size >= 1048576 And Size < 1073741824 Then
		GetSize = FormatNumber((Size / 1024) / 1024, 2) & "MB"
	ElseIf Size >= 1073741824 Then
		GetSize = FormatNumber((Size / 1024) / 1024 / 1024, 2) & "GB"
	End If
End Function
%>

<%
'==================================================================================
'=================================列出select option=================================
'=================================2017/04/22========================================
function options(m_id) '模块id
SQL="Select * from tree_list where class_module=" & m_id &" and class_enable=1 order by class_path "     'class_enable=ture
Set rs=server.CreateObject("ADODB.RecordSet")
rs.Open SQL,conn,1,1
do while not rs.eof
	c_id = rs("id")
	c_name = rs("Class_name")
	c_enable = rs("Class_enable")
	c_path = rs("Class_path")
	c_arr = split(c_path,",") 'split()函数，将字符串用分割符号切割成数组
	c_num = ubound(c_arr) 'ubound()函数，检测出数组元素的数目
	str=""
	for i=2 to c_num
		str= str & "├" & convert_id_to_name(c_arr(i))
	next
	        options=options & response.Write("<option value="& c_id &">" & str & "</option>")
	rs.movenext
loop
rs.close
set rs=nothing
end function
%>
<%
'===============================================================
'==================获得参数对应的类别名称 class_name================
'====================2017/04/22==================================
function convert_id_to_name(id)
   sql_f ="select * from file_list_class where ID = "& id 
   Set rs_f = Server.CreateObject ("ADODB.Recordset")
   rs_f.Open sql_f,conn,1,1
   if not rs_f.eof then
	   convert_id_to_name = rs_f("class_name")
   end if
   rs_f.close
   set rs_f=nothing
end function
%>

<% 
'===========================================================================
'============================生成随机数=====================================
'===========================================================================
Function randKey(obj) 
 Dim char_array(80) 
 Dim temp 
 For i = 0 To 9  
  char_array(i) = Cstr(i) 
 Next 
 For i = 10 To 35 
  char_array(i) = Chr(i + 55) 
 Next 
 For i = 36 To 61 
  char_array(i) = Chr(i + 61) 
 Next 
 Randomize 
 For i = 1 To obj 
  temp = temp&char_array(int(62 - 0 + 1)*Rnd + 0) 
 Next 
 randKey = temp 
End Function 
%> 


<% 
'===========================================================================
'============================获取符合条件的记录个数 ============================
'====sql-条件
'===========================================================================
Function rec_count(sql_rec_count) 
   Set rs_rec_count = Server.CreateObject ("ADODB.Recordset")
   rs_rec_count.open sql_rec_count,conn,1,1
   rec_count=rs_rec_count.recordcount
   rs_rec_count.close
End Function 
%> 


<% 
'===========================================================================
'============================根据条件获取字段值============================
'====t-表名 f -字段 v -条件 z-要获取的值
'===========================================================================
Function trans(t,f,v,z)  '表-条件-条件值-返回值
   if v="" then
      trans="#ERROR#"
	  v=0
      exit function
   end if
   Set rsr = Server.CreateObject ("ADODB.Recordset")
   sqlr="select * from " & t & " where "& f & " = "& v 
   rsr.open sqlr ,conn,1,1
   if not rsr.eof then 
       trans=rsr(z)
   else
       trans="#ERROR#"
   end if
   rsr.close
End Function 
%> 

<% 
'===========================================================================
'============================条件判断============================
'====
'===========================================================================
Function iif(iifa,iifb,iifc) 
   if iifa=true then iif=iifb
   if iifa=false then iif=iifc
End Function 
%> 