﻿<%Session.CodePage=65001%>
<%Response.Charset="utf-8"%>
<%
htmlData = Request.Form("content1")
Function htmlspecialchars(str)
	str = Replace(str, "&", "&amp;")
	str = Replace(str, "<", "&lt;")
	str = Replace(str, ">", "&gt;")
	str = Replace(str, """", "&quot;")
	htmlspecialchars = str
End Function
%>
	<link rel="stylesheet" href="editor/themes/default/default.css" />
	<link rel="stylesheet" href="editor/plugins/code/prettify.css" />
	<script charset="utf-8" src="editor/kindeditor.js"></script>
	<script charset="utf-8" src="editor/lang/zh-CN.js"></script>
	<script charset="utf-8" src="editor/plugins/code/prettify.js"></script>
	<script>
		KindEditor.ready(function(K) {
			var editor1 = K.create('textarea[name="content1"]', {
				cssPath : 'editor/plugins/code/prettify.css',
				uploadJson : 'editor/asp/upload_json.asp',
				fileManagerJson : 'editor/asp/file_manager_json.asp',
				allowFileManager : false,
		})});
	</script>

<div class="container">
<div class="path"><img src="images/home.gif" width="14" height="14" />>在线投稿</div><!--path-->
	<%'=htmlData%>
    <%'=htmlspecialchars(htmlData)%>
<form name="xx" method="post" action="" class="table_tg">
<table width="100%" border="1">
  <tr>
    <th width="8%" scope="col"><div align="center">文章标题</div></th>
    <td width="56%" scope="col"><input name="title" type="text" size="50" style="width:100%"></td>
    <td width="36%" scope="col">*文章标题</td>
  </tr>
  <tr>
    <th scope="row"><div align="center">文章类型</div></th>
    <td><select name="class" style="width:100%"><option value="">===========请选择知识类别==========</option><%call options(1)%></select></td>
    <td>*文章类型，注意：只能选择二级类型</td>
  </tr>
  <tr>
    <th scope="row"> <div align="center">文章标签</div></th>
    <td><input name="keywords" type="text" size="50" style="width:100%"></td>
    <td>*增加标签有助于快速检索到文章，标签间用“|”隔开</td>
  </tr>
  <tr>
    <th scope="row"><div align="center">文章提要</div></th>
    <td><textarea name="abstract" cols="45" rows="4" style="width:100%;" resize=none;></textarea></td>
    <td>*不要超过200字符</td>
  </tr>
  <tr>
    <th scope="row"><div align="center">正文</div></th>
    <td colspan="2"><textarea name="content1" style="width:100%;height:500px;visibility:hidden;"></textarea></td>
  </tr>
  <tr>
    <th colspan="3" scope="row" align="center"><input type="submit" name="button" value="提交内容" /></th>
    </tr>
</table>
</form>
</div>
</body>
</html>
<%
tj =false
if request.Form("button")<>"" then tj=true
if tj then
      set rs=server.CreateObject("adodb.recordset")
      sql ="select * from zs_list "
      rs.open sql,conn,1,3
      rs.addnew
      rs("zs_title")=request("title")
	  rs("zs_class")=request("class")
	  rs("zs_addtime")=now()
	  rs("zs_adduser")=session("m_name")
	  rs("zs_tag")=request("keywords")
	  rs("zs_abstract")=request("abstract")
	  rs("zs_content")=htmlData
      rs.update
	  response.write ("<script>alert('投稿成功,待管理员审核...');</script>")
end if
%>
<%
'=================================列出select option==============================
function options(m_id) '模块id
SQL="Select * from zs_class where class_module=" & m_id &" and class_enable=true order by class_path "
Set rs=server.CreateObject("ADODB.RecordSet")
rs.Open SQL,conn,1,1
do while not rs.eof
	c_id = rs("id")
	c_name = rs("Class_name")
	c_enable = rs("Class_enable")
	c_path = rs("Class_path")
	c_arr = split(c_path,",") 'split()函数，将字符串用分割符号切割成数组
	c_num = ubound(c_arr) 'ubound()函数，检测出数组元素的数目
	str=""
	for i=2 to c_num
		str=str & "├" & convert_id_to_name(c_arr(i))
	next
	        options=options & response.Write("<option value="& c_id &">" & str & "</option>")
	rs.movenext
loop
rs.close
set rs=nothing
end function
'==================获得参数对应的类别名称 class_name================
function convert_id_to_name(idd)
   sql_f ="select * from zs_class where ID = "& idd
   Set rs_f = Server.CreateObject ("ADODB.Recordset")
   rs_f.Open sql_f,conn,1,1
   if not rs_f.eof then
	   convert_id_to_name = rs_f("class_name")
   end if
   rs_f.close
   set rs_f=nothing
end function
%>
